" vim: ts=4 fdm=marker

" About {{{1
"   Author: Charles E. Campbell, Jr.
"   Maintainer: dsjkvf@gmail.com
"   Date:   Aug 18, 2008
"   Version: 3.1
"   Notes: Phillippians 1:27
"       Only let your manner of life be worthy of the gospel of
"       Christ, that, whether I come and see you or am absent, I may hear of
"       your state, that you stand firm in one spirit, with one soul striving
"       for the faith of the gospel

" Load Once {{{1
if exists("g:loaded_RltvNmbrPlugin")
    finish
endif
let g:loaded_RltvNmbrPlugin = "v3"

" Check dependencies {{{1
if v:version < 700
    echoerr "Sorry, this version of RltvNmbr needs Vim 7.0"
    finish
endif
if !has("signs")
    echoerr 'Sorry, your Vim is missing +signs; use "configure --with-features=huge" , recompile, and install'
    finish
endif
if !has("syntax")
    echoerr 'Sorry, your Vim is missing +syntax; use "configure --with-features=huge" , recompile, and install'
    finish
endif
let s:keepcpo= &cpo
set cpo&vim

" Public Interface {{{1
" Commands {{{2
com! -bang RltvNmbr call RltvNmbr#RltvNmbrCtrl(<bang>1)
com! -nargs=0 RN call RltvNmbr#RltvNmbrToggle()

" GUI Menu {{{2
let s:RLTVNMBR= 2683
if has("gui_running") && has("menu") && &go =~ 'm'
    if !exists("g:DrChipTopLvlMenu")
        let g:DrChipTopLvlMenu= "DrChip."
    endif
    exe "menu ".g:DrChipTopLvlMenu."RltvNmbr.Start<tab>:RltvNmbr    :RltvNmbr<cr>"
endif

"  Restore {{{1
let &cpo= s:keepcpo
unlet s:keepcpo
