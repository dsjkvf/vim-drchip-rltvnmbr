vim-drchip-rltvnmbr
===================

## About

This is a Vim plugin for displaying the line numbering relative to the current line. Originally written by [Charles Campbell](www.drchip.org/astronaut/), copied now from [his collection](www.drchip.org/astronaut/vim/index.html#RLTVNMBR) of his Vim-related files to a separate repository in order to make the installation process easier.
